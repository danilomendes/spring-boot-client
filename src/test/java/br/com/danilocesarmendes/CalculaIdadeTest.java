package br.com.danilocesarmendes;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.Period;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CalculaIdadeTest {


		@Test
		void calculaIdade1985() {
			
			assertEquals(this.calculaIdade(LocalDate.of(1985, 1, 31)), 35);
			
		}
		
		@Test
		void calculaIdade2018() {
			
			assertEquals(this.calculaIdade(LocalDate.of(2018, 8, 07)), 1);
			
		}
		
		public int calculaIdade(final LocalDate aniversario) {
		    return Period.between(aniversario, LocalDate.of(2020, 7, 21)).getYears();
		}

	}