package br.com.danilocesarmendes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.danilocesarmendes.domain.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{

}
