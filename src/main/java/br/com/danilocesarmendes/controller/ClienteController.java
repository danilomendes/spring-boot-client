package br.com.danilocesarmendes.controller;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import br.com.danilocesarmendes.domain.Cliente;
import br.com.danilocesarmendes.service.ClienteService;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@GetMapping("/clientes")
	public ResponseEntity<Map<String, Object>> getAllClientesPage(@RequestParam(required = false) String title,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size,
			@RequestParam(defaultValue = "id,desc") String[] sort) {

		try {

			List<Cliente> clientes = new ArrayList<Cliente>();

			Page<Cliente> pageClientes;
			pageClientes = clienteService.getClientes(page, size, sort);

			clientes = pageClientes.getContent();

			if (clientes.isEmpty()) {
				Map<String, Object> response = new HashMap<>();
				response.put("records", clientes);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}

			Map<String, Object> response = new HashMap<>();

			response.put("total_elements", pageClientes.getTotalElements());
			response.put("size", size);
			response.put("current_page", pageClientes.getNumber() + 1);
			response.put("last_page", pageClientes.getTotalPages());
			if (page + 1 <= pageClientes.getTotalPages()) {
				response.put("next_page_url",
						"http://localhost:8080/api/clientes?page=" + (page + 1) + "&size=" + size);
			}
			if (page - 1 > 0) {
				response.put("prev_page_url",
						"http://localhost:8080/api/clientes?page=" + (page - 1) + "&size=" + size);
			}
			response.put("records", clientes);

			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/clientes/{id}")
	public ResponseEntity<Cliente> getById(@PathVariable("id") Long id) {
		Optional<Cliente> clientes = clienteService.getById(id);

		if (clientes.isPresent()) {
			return new ResponseEntity<>(clientes.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/clientes")
	public ResponseEntity<Cliente> adicionaCliente(@Valid @RequestBody Cliente entity) {
		try {
			Cliente cliente = clienteService.create(entity);
			return new ResponseEntity<>(cliente, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@PutMapping("/clientes/{id}")
	public ResponseEntity<Cliente> updateCliente(@PathVariable("id") long id, @RequestBody Cliente entity) {
		Optional<Cliente> clienteDB = clienteService.getById(id);

		if (clienteDB.isPresent()) {
			Cliente cliente = clienteDB.get();
			cliente.setNome(entity.getNome());
			cliente.setCpf(entity.getCpf());
			cliente.setDataNascimento(entity.getDataNascimento());
			return new ResponseEntity<>(clienteService.create(cliente), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PatchMapping("/clientes/{id}")
	public ResponseEntity<Cliente> updateClientePatch(@PathVariable long id, @RequestBody Map<String, Object> campos) {

		Optional<Cliente> clienteDB = clienteService.getById(id);

		if (clienteDB.isPresent()) {

			Cliente cliente = clienteDB.get();

			return new ResponseEntity<>(clienteService.update(cliente, campos), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/clientes/{id}")
	public ResponseEntity<HttpStatus> delete(@PathVariable("id") long id) {
		try {
			clienteService.delete(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}

}
