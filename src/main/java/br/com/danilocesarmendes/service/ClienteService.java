package br.com.danilocesarmendes.service;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import br.com.danilocesarmendes.domain.Cliente;
import br.com.danilocesarmendes.repository.ClienteRepository;

@Service
public class ClienteService {

	private final ClienteRepository repository;

	@Autowired
	public ClienteService(ClienteRepository clienteRepository) {
		this.repository = clienteRepository;
	}

	public Cliente create(Cliente cliente) {
		return repository.save(cliente);
	}

	public Cliente update(Cliente cliente, Map<String, Object> campos) {
		this.merge(campos, cliente);

		return repository.save(cliente);
	}

	public void delete(Long id) {
		Cliente cliente = repository.getOne(id);
		repository.delete(cliente);
	}

	public Page<Cliente> getClientes(int page, int size, String[] sort) {
		List<Order> orders = this.getOrders(sort);

		Pageable pageable = PageRequest.of(page == 0 ? page : page - 1, size, Sort.by(orders));

		Page<Cliente> list = repository.findAll(pageable);

		list.forEach(obj -> {
			obj.setIdade(this.calculaIdade(obj.getDataNascimento()));
		});

		return list;
	}

	public int calculaIdade(final LocalDate aniversario) {
	    return Period.between(aniversario, LocalDate.now()).getYears();
	}

	private List<Order> getOrders(String[] sort) {
		List<Order> orders = new ArrayList<Order>();
		if (sort[0].contains(",")) {
			// will sort more than 2 fields
			// sortOrder="field, direction"
			for (String sortOrder : sort) {
				String[] _sort = sortOrder.split(",");
				orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
			}
		} else {
			// sort=[field, direction]
			orders.add(new Order(getSortDirection(sort[1]), sort[0]));
		}

		return orders;
	}

	private Sort.Direction getSortDirection(String direction) {
		if (direction.equals("asc")) {
			return Sort.Direction.ASC;
		} else if (direction.equals("desc")) {
			return Sort.Direction.DESC;
		}

		return Sort.Direction.ASC;
	}

	public Optional<Cliente> getById(Long id) {
		Optional<Cliente> entity = repository.findById(id);
		
		if(entity.isPresent()) {
			entity.get().setIdade(this.calculaIdade(entity.get().getDataNascimento()));
		}
		
		return entity;
	}

	public void merge(Map<String, Object> camposOrigem, Object destino) {
		try {
			ObjectMapper mapper = objectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, true);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);

			Cliente clienteOrigem = mapper.convertValue(camposOrigem, Cliente.class);

			camposOrigem.forEach((nomePropriedade, valorPropriedade) -> {

				if (valorPropriedade != null) {

					System.out.println(nomePropriedade);

					Field field = ReflectionUtils.findField(Cliente.class, nomePropriedade);

					if (field != null) {
						field.setAccessible(true);

						Object valor = ReflectionUtils.getField(field, clienteOrigem);

						ReflectionUtils.setField(field, destino, valor);
					}
				}
			});
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	public static ObjectMapper objectMapper() {
		JavaTimeModule module = new JavaTimeModule();
		LocalDateTimeDeserializer localDateTimeDeserializer = new LocalDateTimeDeserializer(
				DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		module.addDeserializer(LocalDateTime.class, localDateTimeDeserializer);
		ObjectMapper objectMapperObj = Jackson2ObjectMapperBuilder.json().modules(module)
				.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS).build();
		return objectMapperObj;
	}

}
