# Platform Builders #
## API REST - CRUD DE CLIENTES ##

Para apreciação da API utilize o import do arquivo PlatformBuilders.postman_collection.json 

### Quais tecnologias encontra-se esse projeto? ###
* [Java 8](http://www.java.com)
* [SpringBoot](https://spring.io/)

### Requisitos de instalação ###
* [JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Maven](http://maven.apache.org/)

## Start ##
* Comandos basicos do maven
* mvn package
* java -jar target/spring-boot-client-0.0.1-SNAPSHOT.jar


## Contato ##
- [LinkedIn](https://www.linkedin.com/in/danilo-cesar-mendes/)
- [WhatsApp](https://api.whatsapp.com/send?phone=5516991001800)

